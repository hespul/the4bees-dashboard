# The4Bees dashboard

## Demo 

https://the4bees.hespul.org/

-------------------------------

## Requirements

### For production use

You just need a web server to serve the files built with Vue.

### For development

You need:

* Docker
* Python 3.6 + pip
* Invoke

## Installation

### For development

Install Invoke :

    pip3.6 install invoke

Build docker images:

    inv vue.install

Run dev server :

    inv up

You should be able to access the dashboard from http://127.0.0.1:8080

Application files are under `./dashboard/src/`

### Build for production

To build production files:

    inv vue.build

Source files are located in `./dashboard/dist/` copy everything to you web server. It should work.

-------------------------------

## Use the application 

You can display one or several dashboards on the same page depending of url arguments.

This will display one dashboard titled "HESPUL" with data coming from `ds_Fr69_hespul__2356` dataset:

* https://the4bees.hespul.org/?installation=ds_Fr69_hespul__2356&dashboard_name=HESPUL

This will alternate the display between two dashboard titled "HESPUL" and
"THE4BEES" with data coming from `ds_Fr69_hespul__2356`dataset for HESPUL and
`ds_Fr69_hespul_strm_sn2_5133` for THE4BEES:

* https://the4bees.hespul.org/?dashboard_name=HESPUL,THE4BEES&installation=ds_Fr69_hespul__2356,ds_Fr69_hespul_strm_sn2_5133


Once called with arguments, the application use browser's local storage to keep
dashboard_name and installation. So you can omit then after.


On phones, Firefox and Chrome allows the application to be installed on the phone as a web app.

-------------------------------

## Use as a kiosk for displaying it fullscreen unattended

To be able to display the dashboard in fullscreen mode with a low power
computer, we use a Raspberry Pi with Raspbian OS.

You will need to install Firefox:

    sudo bash
    apt install firefox unclutter

Launch Firefox and install the AutoFullscreen extension (source:
http://github.com/tazeat/AutoFullscreen). Add your url as your homepage in
preferences.

And add script to run Firefox at boot in file `/home/pi/.config/lxsession/LXDE-pi/autostart`:

    @unclutter -idle 10
    @/usr/bin/firefox

Add argument 'kiosk_mode' to url

* https://the4bees.hespul.org/?dashboard_name=HESPUL,THE4BEES&installation=ds_Fr69_hespul__2356,ds_Fr69_hespul_strm_sn2_5133&kiosk_mode=1


-------------------------------

Bertheliet
https://the4bees.hespul.org/?dashboard_name=SN7,SN8&installation=ds_Fr69_hespul_strm_sn7_5042,ds_Fr69_hespul_strm_sn8_5151

-------------------------------


To use HESPUL datastore, those http headers are needed (in /admin/http_headers/httpheader/) :

    Access-Control-Allow-Headers X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding
    Access-Control-Allow-Methods *
    Access-Control-Allow-Origin *
