var path = require('path')
var webpack = require('webpack')

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: 'build.js'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                    }
                    // other vue-loader options go here
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        }
    },
    devServer: {
        // host: '0.0.0.0',
        // port: '8080',
        historyApiFallback: true,
        disableHostCheck: true,
        //public: 'the4bees.hespul.org',
        noInfo: true,
    },
    performance: {
        hints: false
    },
    devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
    module.exports.devtool = '#source-map'
    // http://vue-loader.vuejs.org/en/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"',
                npm_config_report: true,
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            ecma: 8,
            warnings: true,
            keep_classnames: true,
            keep_fnames: true,
            safari10: true,
            compress: {
                warnings: true //false
            },
            output: {
                comments: false,
                beautify: false,
            },
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: false //true
        }),
        new UnminifiedWebpackPlugin(),
    ])
}
