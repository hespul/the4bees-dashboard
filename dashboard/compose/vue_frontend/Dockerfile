# Build vue.js
FROM node:slim
MAINTAINER Alexandre Norman <alexandre.norman@hespul.org>

RUN mkdir /code
COPY . /code
WORKDIR /code

RUN npm install --global \
    webpack \
    webpack-bundle-analyzer \
    webpack-dev-server \
    webpack-merge \
    unminified-webpack-plugin \
    vue-loader \
    vue-style-loader \
    vue-template-compiler \
    ajv \
    autoprefixer \
    babel-core \
    babel-eslint \
    babel-helper-vue-jsx-merge-props \
    babel-loader \
    babel-plugin-syntax-jsx \
    babel-plugin-transform-runtime \
    babel-plugin-transform-vue-jsx \
    babel-preset-env \
    babel-preset-stage-2 \
    chalk \
    copy-webpack-plugin \
    coreapi \
    css-loader \
    eslint \
    eslint-config-standard \
    eslint-friendly-formatter \
    eslint-loader \
    eslint-plugin-import \
    eslint-plugin-node \
    eslint-plugin-promise \
    eslint-plugin-standard \
    eslint-plugin-vue \
    extract-text-webpack-plugin \
    file-loader \
    friendly-errors-webpack-plugin \
    html-webpack-plugin \
    node-notifier \
    optimize-css-assets-webpack-plugin \
    ora \
    portfinder \
    postcss-import \
    postcss-loader \
    postcss-url \
    rimraf \
    semver \
    shelljs \
    uglifyjs-webpack-plugin \
    url-loader \
    vue-chartjs \
    chart.js

# RUN export NODE_ENV=production && npm install && npm run build
RUN npm install 
RUN export NODE_ENV=production && npm run build

# Build CSS
FROM registry.hespul.org:5000/docker-assetscompiler:latest
COPY src/stylesheet /stylus
RUN /usr/local/bin/gulp mimify:stylus build:stylus


# Build final nginx image
FROM nginx:latest
MAINTAINER Webmestre HESPUL <webmestre@hespul.org>

ADD compose/vue_frontend/nginx-prod.conf /etc/nginx/nginx.conf

RUN mkdir /app
WORKDIR /app
COPY --from=0 /code/dist /app
COPY --from=1 /stylus_css /app/static/css
