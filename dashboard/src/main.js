// import Vue from 'vue/dist/vue.js'     // dev mode
//import 'babel-polyfill'
import Vue from 'vue/dist/vue.common.js'
import VueResource from 'vue-resource'
import _ from 'lodash'
import VueHighcharts from 'vue-highcharts';

import Dashboard from './components/dashboard.vue';
import MultipleDashboards from './components/multiple_dashboards.vue';

Vue.use(VueResource);
Vue.use(VueHighcharts);

Vue.config.devtools = true;
// Vue.config.productionTip = false;
Vue.config.productionTip = true;


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


// call http://172.16.1.7:8080/?installation=ds_Fr69_hespul__2356&dashboard_name=HESPUL
// call http://127.0.0.1:8080/?dashboard_name=HESPUL,THE4BEES&installation=ds_Fr69_hespul__2356,ds_Fr69_hespul_strm_sn2_5133
// ?kiosk_mode=1

// http://api.smartdatanet.it/api/ds_Fr69_hespul__2356/Measures?$format=json&$top=999&$orderby=time%20desc
// http://api.smartdatanet.it/api/ds_Fr69_hespul__2356/MeasuresStats?$format=json&$top=200&timeGroupBy=hour&timeGroupOperators=avg,T
// http://api.smartdatanet.it/api/ds_Trfl_2/Measures?$filter=time ge datetimeoffset'2018-07-01T07:00:00+01:00' and time lt datetimeoffset'2018-12-01T07:15:00+01:00'




//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/Measures?$format=json&$filter=temp gt 25 and temp lt 27&$top=15&$orderby=temp%20desc
//  
//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/MeasuresStats?$format=json&$filter=month%20gt%202%20and%20month%20lt%207&$top=15&timeGroupBy=month_year&timeGroupOperators=avg,temp
//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/MeasuresStats?$format=json&$filter=month%20gt%202&$top=15&timeGroupBy=month_year&timeGroupOperators=avg,temp
//  
//  
//  OK :
//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/MeasuresStats?$format=json&$top=15&timeGroupBy=month_year&timeGroupOperators=avg,temp
// 
// 
// NOK :
//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/MeasuresStats?$format=json&$filter=month%20gt%202%20and%20year%20eq%202018&$top=15&timeGroupBy=month_year&timeGroupOperators=avg,temp
// 
// 
// OK :
//  http://api.smartdatanet.it/api/ds_De_bwcon_sen_1971/MeasuresStats?$format=json&$filter=year%20eq%202018&$top=15&timeGroupBy=month_year&timeGroupOperators=avg,temp

// http://127.0.0.1:8000/datastore/datapoint/?sensor_box_slug=hespul-maite&start_date=2018-08-30

var envs = getUrlVars();
var installation = 'hespul-maite';
var dashboard_name =  'The4Bees';
var kiosk_mode = false;
var server = 'https://the4bees-dashboard.hespul.org';

if (envs.installation != undefined) {
    installation = envs.installation;
}
else if (localStorage.getItem("installation") != null) {
    installation = localStorage.getItem("installation");
}
if (envs.dashboard_name != undefined) {
    dashboard_name = decodeURIComponent(envs.dashboard_name);
}
else if (localStorage.getItem("dashboard_name") != null) {
    dashboard_name = localStorage.getItem("dashboard_name");
}

if (envs.server != undefined) {
    server = envs.server;
}
else if (localStorage.getItem("server") != null) {
    server = localStorage.getItem("server");
}


if (envs.kiosk_mode != undefined) {
    kiosk_mode = true;
}

localStorage.setItem("installation", installation);
localStorage.setItem("dashboard_name", dashboard_name);


var dashboards = [];
var installations = installation.split(',');
var dashboards_names = dashboard_name.split(',');


for (var id in installations) {
    var base_url = server + '/datastore/datapoint/?sensor_box_slug='+ installations[id];
    var base_url_avg = server + '/datastore/datapoint-avg-day/?sensor_box_slug='+ installations[id];
    dashboards.push(
        {
            base_url: base_url,
            base_url_avg: base_url_avg,
            dashboard_name: dashboards_names[id],
            id: id,
        },
    );
}

const app_dashboards = new Vue(
    {
        el: '#app',
        data: {
            name: 'app',
        },
        render: (h) => h(
            MultipleDashboards,
            {
                ref: 'app',
                props: {
                    dashboards: dashboards,
                    kiosk_mode: kiosk_mode,
                }
            }
        ),
    }
);

