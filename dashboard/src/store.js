import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// // root state object.
// // each Vuex instance is just a single state tree.
// const state = {
//     // counter
//     count: 10,
//     
//     // map
//     latitude: null,
//     latitude_old: null,
//     longitude: null,
//     longitude_old: null,
//     
//     // installation
//     tilt: null,
//     tilt_old: null,
//     orientation: null,
//     orientation_old: null,
//     power: null,
//     power_old: null,
//     productivity: null,
//     subscription: null,
//     autoconsumption: null,
// 
//     // electricity prices
//     current_price: null,
//     price_inflation_percent_for_5_years: null,
//     price_inflation_percent_after_5_years: null,
// 
//     // investment
//     investment_pv_installation: null,
//     investment_pv_installation_old: null,
//     investment_iwb: null,
//     investment_overlay: null,
//     investment_surplus_sale: null,
//     investment_free_sale: null,
//     investment_autoconsumption: null,
// 
//     // Coreapi
//     coreapi_client: null,
// }
// 
// // mutations are operations that actually mutates the state.
// // each mutation handler gets the entire state tree as the
// // first argument, followed by additional payload arguments.
// // mutations must be synchronous and can be recorded by plugins
// // for debugging purposes.
// const mutations = {
//     increment (state) {
//         state.count++;
//     },
//     decrement (state) {
//         state.count--;
//     },
//     // increment: state => state.count++,
//     // decrement: state => state.count--,
//     updateCount  (state, message) {
//         state.count = parseInt(message);
//     },
//     
//     setLatitude (state, latitude) {
//         state.latitude = latitude;
//     },
//     setLongitude (state, longitude) {
//         state.longitude = longitude;
//     },
//     setPosition (state, latitude, longitude) {
//         state.latitude = latitude;
//         state.longitude = longitude;
//     },
// }
// 
// // actions are functions that cause side effects and can involve
// // asynchronous operations.
// const actions = {
// //    setLatitude: ({ commit }) => commit('setLatitude'),
// //     increment: ({ commit }) => commit('increment'),
// //    decrement: ({ commit }) => commit('decrement'),
// //   incrementIfOdd ({ commit, state }) {
// //     if ((state.count + 1) % 2 === 0) {
// //       commit('increment')
// //     }
// //   },
// //   incrementAsync ({ commit }) {
// //     return new Promise((resolve, reject) => {
// //       setTimeout(() => {
// //         commit('increment')
// //         resolve()
// //       }, 1000)
// //     })
// //   }
// }
// 
// // getters are functions
// const getters = {
//     //   evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd'
//     getCounter: state => state.count,
//     getLatitude: state => state.latitude,
//     getLongitude: state => state.longitude,
// }

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
const store =  new Vuex.Store({
    state: {
        count: 10,
    },
    getters: {
        getCounter: state => state.count,
    },
    // actions: {
    // },
    mutations: {
        increment: state => state.count++,
        decrement: state => state.count--,
        updateCount  (state, message) {
            state.count = parseInt(message);
            // Vue.set(state, 'count', parseInt(message));
        },
    },
})

export default store;
