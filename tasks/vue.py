from invoke import task
from .docker import dc

@task(help={
    'service': 'service name [djcms]',
})
def install(ctx, service=None):
    """
    Install npm dependencies in vue_cli container
    """
    service = service or ctx.service
    dc(ctx, service, 'run --rm vue_cli npm install')


@task(help={
    'service': 'service name [djcms]',
})
def build(ctx, service=None):
    """
    Build production vue application
    """
    service = service or ctx.service
    dc(ctx, service, 'run --rm vue_cli bash -c \'NODE_ENV=production npm run build --mode production\'')

