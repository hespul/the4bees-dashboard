from invoke import task


@task(help={
    'service': 'service name [djcms]',
})
def prod(ctx, service=None):
    """
    Build vue application and publish for HESPUL production
    """
    service = service or ctx.service
    from .vue import build
    build(ctx, service)

    cmd = 'cd dashboard/dist/ && rsync -avr * root@the4bees-dashboard.hespul.org:/docker/the4bees-dashboard/dashboard/dist/'
    ctx.run(cmd, pty=True)

