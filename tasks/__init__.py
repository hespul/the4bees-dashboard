from invoke import Collection

import tasks.docker
import tasks.vue
import tasks.publish

ns = Collection(
    docker,
    docker.up,
    docker.upd,
    docker.down,
    docker.restart,
    vue,
    publish
)


def get_project_name():
    import os
    try:
        return os.getcwd().split(os.path.sep)[-1]
    except:
        return None


ns.configure(
    {
        'service':  get_project_name() or 'the4bees',
        'cont': '',
        'registry': 'registry.hespul.org:5000',
        'path_to_compose': 'compose/envs/',
        'domain': 'the4bees.hespul.org',
    }
)
