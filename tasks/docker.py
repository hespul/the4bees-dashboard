import os
from invoke import task


def dc(ctx, service=None, command='', path_to_compose=None, pty=False):
    service = service or ctx.service
    path_to_compose = path_to_compose or ctx.path_to_compose

    cmd = 'export APP_VERSION=$(date +%Y%m%d%H%M); docker-compose -f {0}.yml -p {1} {2}'.format(
        os.path.join(path_to_compose, service),
        service,
        command
    )
    ctx.run(cmd, pty=pty)


# @task()
# def set_volumes(ctx):
#     """
#     Create needed custom volumes in docker
#     """
#     volumes = [
#         'explaura_app_django',
#     ]
#     for vol in volumes:
#         try:
#             print('Create volume: ', vol)
#             ctx.run('docker volume create --name={}'.format(vol))
#         except:
#             pass
# 
    
@task()
def set_networks(ctx):
    """
    Create needed custom networks in docker
    """
    networks = [
        'frontend',
        'explaura',
        'phplist',
    ]
    for net in networks:
        try:
            print('Create network: ', net)
            ctx.run('docker network create {}'.format(net))
        except:
            pass


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
})
def up(ctx, service=None, container=''):
    """
    Launch service container
    """
    service = service or ctx.service
    dc(ctx, service, 'up {}'.format(container))


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
})
def upd(ctx, service=None, container=''):
    """
    Launch service container and detach
    """
    service = service or ctx.service
    dc(ctx, service, 'up -d {}'.format(container))


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
})
def down(ctx, service=None, container=''):
    """
    Shutdown service container
    """
    service = service or ctx.service
    if container:
        dc(ctx, service, 'stop {}'.format(container))
        dc(ctx, service, 'rm -f {}'.format(container))
    else:
        dc(ctx, service, 'down')

@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
})
def restart(ctx, service=None, container=''):
    """
    Restart service container
    """
    service = service or ctx.service
    down(ctx, service, container)
    upd(ctx, service, container)


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [all]',
})
def ps(ctx, service=None, container=''):
    """
    ps service container
    """
    service = service or ctx.service
    dc(ctx, service, 'ps {}'.format(container))





@task(help={
    'service': 'service name [djcms]',
    'container': 'container',
    'command': 'command to run'
})
def run(ctx, container, service=None, command=''):
    """
    Run command in service container
    """
    service = service or ctx.service
    dc(ctx, service, 'run --rm {} {}'.format(container, command))
        


@task(help={
    'service': 'service name [djcms]',
    'container': 'container [django]',
})
def bash(ctx, service=None, container='django'):
    """
    Run bash in service container
    """
    service = service or ctx.service
    dc(ctx, service, 'exec  {} /bin/bash'.format(container), pty=True)
        

@task(help={
    'service': 'service name [djcms]',
    'container': 'container [django]',
})
def logs(ctx, service=None, container='django'):
    """
    Show logs for container in service
    """
    service = service or ctx.service
    dc(ctx, service, 'logs  {}'.format(container), pty=True)
